#pragma once

#include <QtCore>

#include "../../Common/DataExtention/idataextention.h"
#include "../../Common/Plugin/referenceinstance.h"

#include "plugin.h"


class ExtendableDataModelFilter : public IExtendableDataModelFilter
{
	Q_OBJECT
	Q_INTERFACES(IExtendableDataModelFilter)
	Q_PROPERTY(int parentId READ parentId NOTIFY parentIdChanged)

public:
	ExtendableDataModelFilter(QObject* parent, ExtendableDataModel* model);
	virtual ~ExtendableDataModelFilter() = default;

	// QAbstractProxyModel interface
public:
	Q_INVOKABLE QModelIndex mapToSource(const QModelIndex& proxyIndex) const override;

	// IExtendableDataModel interface
public:
	ExtendableItemDataMap getHeader() override;
	const ExtendableItemDataMap& getDefaultItem() override;
	int getItemId(const QModelIndex &index) override;
	const ExtendableItemDataMap& getItem(const QModelIndex &index) override;
	const ExtendableItemDataMap& getItem(int itemId) override;
	bool hasItem(int itemId) override;
	QList<int> getItemIds() override;

public slots:
	QVariantMap getVariantDefaultItem() override;
	QVariantMap getVariantItem(int itemId) override;
	QVariantMap getFirstVariantItem() override;
	const ExtendableItemDataMap& getFirstItem() override;
	QModelIndex getItemIndex(int itemId) override;
	int appendItem(const ExtendableItemDataMap& itemValues) override;
	int addItem(const ExtendableItemDataMap& itemValues, int row, QModelIndex parentIndex) override;
	int addVariantItem(const QVariantMap& itemValues, int row, int parentId) override;
	bool updateItem(int itemId, const ExtendableItemDataMap& itemValues) override;
	bool updateVariantItem(int itemId, const QVariantMap& itemValues) override;
	bool updateItem(const QModelIndex &index, const ExtendableItemDataMap& itemValues) override;
	bool removeItem(int itemId) override;
	bool removeItem(const QModelIndex &index) override;

signals:
	void headerChanged() override;
	void itemAdded(int itemId) override;
	void itemUpdated(int itemId, const ExtendableItemDataMap& before, const ExtendableItemDataMap& after) override;
	void itemRemoved() override;
	void modelChanged() override;
	void parentIdChanged(int parentId) override;

public:
	void setColumns(QMap<Interface, QStringList> filteredColumns) override;
	void setTreeParent(QModelIndex parentIndex) override;
	void setTreeParent(int parentRow) override;
	int parentId() const override
	{
		return m_parentId;
	}

	// QSortFilterProxyModel interface
protected:
	bool filterAcceptsRow(int source_row, const QModelIndex& source_parent) const override;
	bool filterAcceptsColumn(int source_column, const QModelIndex& source_parent) const override;

private:
	ExtendableDataModel* m_model;
	QMap<Interface, QStringList> m_filteredColumns;

	bool m_isParentSet;
	QModelIndex m_filterParentIndex;
	int m_parentId;
};
