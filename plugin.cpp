#include "plugin.h"

ExtendableDataManager::ExtendableDataManager() :
	QObject(),
	PluginBase(this)
{
	m_dataExtentions.reset(new ReferenceInstancesList<IDataExtention>());
	initPluginBase({
		{INTERFACE(IPlugin), this}
	},
	{
		{INTERFACE(IAsyncDataBase), m_dataSource},
	},
	{
		{INTERFACE(IDataExtention), m_dataExtentions},
	});
}

ExtendableDataManager::~ExtendableDataManager()
{
}

void ExtendableDataManager::onReferencesSet()
{
	//	m_dataSource->SetPassword("rqCZB63Fr7tmTB");
}

void ExtendableDataManager::onReferencesListUpdated(Interface interface)
{
	Q_UNUSED(interface)

	QMap<Interface, ExtendableItemDataMap> extentions;
	for(auto& extention : *m_dataExtentions.data())
	{
		QMap<QString, QVariant> fields;
		auto fieldNames = extention->getDataFields();
		auto&& extentionObject = extention.reference()->object();
		bool isValid = true;
		for(auto& fieldName : fieldNames)
		{
			auto fieldProperty = extentionObject->property(fieldName.toUtf8());
			if(!fieldProperty.isValid())
			{
				qDebug() << QString("ERROR: can't find property %1 of extention %2").arg(fieldName).arg(extention.reference()->name());
				isValid = false;
				break;
			}
			fields[fieldName] = fieldProperty;
		}
		if(isValid)
		{
			ExtendableItemDataMap& extentionsList = extentions[extention->getExtendableInterface()];
			extentionsList.insert(extention->getExtentionInterface(), fields);
		}
	}

	for(auto mainIter = extentions.begin(); mainIter != extentions.end(); ++mainIter)
	{
		auto&& interface = mainIter.key();
		if(!mainIter.value().contains(interface))
		{
			qDebug() << QString("ERROR: can't find main extention for %1").arg(interface.iid());
			continue;
		}

		if(!tableHandlers.contains(mainIter.key()))
		{
			tableHandlers[mainIter.key()].reset(new TableHandler(m_dataSource, mainIter.key()));
		}

		for(auto extentionsIter = mainIter.value().begin(); extentionsIter != mainIter.value().end(); ++extentionsIter)
		{
			addExtention(interface, extentionsIter.key(), extentionsIter.value());
		}
	}

	for(auto& extention : *m_dataExtentions.data())
	{
		extention->setModel(tableHandlers[extention->getExtendableInterface()]->GetModel());
	}
}

bool ExtendableDataManager::addExtention(Interface mainInterface, Interface relationInterface, QMap<QString, QVariant> fields)
{
	return tableHandlers[mainInterface]->SetRelation(relationInterface, fields);
}

bool ExtendableDataManager::deleteExtention(Interface mainInterface, Interface relationInterface)
{
	return tableHandlers[mainInterface]->DeleteRelation(relationInterface);
}
