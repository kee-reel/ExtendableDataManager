#include "modelfilter.h"

ExtendableDataModelFilter::ExtendableDataModelFilter(QObject* parent, ExtendableDataModel* model) :
	m_model(model),
	m_isParentSet(false),
	m_parentId(-1)
{
	setSourceModel(model);
	connect(model, &ExtendableDataModel::headerChanged, this, &ExtendableDataModelFilter::headerChanged);
	connect(model, &ExtendableDataModel::itemAdded, this, &ExtendableDataModelFilter::itemAdded);
	connect(model, &ExtendableDataModel::itemUpdated, this, &ExtendableDataModelFilter::itemUpdated);
	connect(model, &ExtendableDataModel::itemRemoved, this, &ExtendableDataModelFilter::itemRemoved);
	connect(model, &ExtendableDataModel::modelChanged, this, &ExtendableDataModelFilter::modelChanged);
}

QModelIndex ExtendableDataModelFilter::mapToSource(const QModelIndex& proxyIndex) const
{
	if(!m_isParentSet)
		return QSortFilterProxyModel::mapToSource(proxyIndex);
	return proxyIndex.isValid() ? QSortFilterProxyModel::mapToSource(proxyIndex) : m_filterParentIndex;
}

ExtendableItemDataMap ExtendableDataModelFilter::getHeader()
{
	return m_model->getHeader();
}

const ExtendableItemDataMap& ExtendableDataModelFilter::getDefaultItem()
{
	return m_model->getDefaultItem();
}

int ExtendableDataModelFilter::getItemId(const QModelIndex& index)
{
	return m_model->getItemId(QSortFilterProxyModel::mapToSource(index));
}

const ExtendableItemDataMap& ExtendableDataModelFilter::getItem(const QModelIndex& index)
{
	return m_model->getItem(QSortFilterProxyModel::mapToSource(index));
}

const ExtendableItemDataMap& ExtendableDataModelFilter::getItem(int itemId)
{
	return m_model->getItem(itemId);
}

bool ExtendableDataModelFilter::hasItem(int itemId)
{
	return m_model->hasItem(itemId);
}

QVariantMap ExtendableDataModelFilter::getVariantItem(int itemId)
{
	return m_model->getVariantItem(itemId);
}

QVariantMap ExtendableDataModelFilter::getFirstVariantItem()
{
	return m_model->getFirstVariantItem();
}

const ExtendableItemDataMap &ExtendableDataModelFilter::getFirstItem()
{
	return m_model->getFirstItem();
}

QModelIndex ExtendableDataModelFilter::getItemIndex(int itemId)
{
	return m_model->getItemIndex(itemId);
}

int ExtendableDataModelFilter::appendItem(const ExtendableItemDataMap& itemValues)
{
	auto result = m_model->appendItem(itemValues);
	invalidate();
	return result;
}

int ExtendableDataModelFilter::addItem(const ExtendableItemDataMap& itemValues, int row, QModelIndex parentIndex)
{
	auto result = m_model->addItem(itemValues, row, QSortFilterProxyModel::mapToSource(parentIndex));
	invalidate();
	return result;
}

int ExtendableDataModelFilter::addVariantItem(const QVariantMap& itemValues, int row, int parentId)
{
	auto result = m_model->addVariantItem(itemValues, row, parentId);
	invalidate();
	return result;
}

bool ExtendableDataModelFilter::updateItem(int itemId, const ExtendableItemDataMap& itemValues)
{
	auto result = m_model->updateItem(itemId, itemValues);
	invalidate();
	return result;
}

bool ExtendableDataModelFilter::updateVariantItem(int itemId, const QVariantMap& itemValues)
{
	auto result = m_model->updateVariantItem(itemId, itemValues);
	invalidate();
	return result;
}

bool ExtendableDataModelFilter::updateItem(const QModelIndex& index, const ExtendableItemDataMap& itemValues)
{
	auto result = m_model->updateItem(QSortFilterProxyModel::mapToSource(index), itemValues);
	invalidate();
	return result;
}

bool ExtendableDataModelFilter::removeItem(int itemId)
{
	auto result = m_model->removeItem(itemId);
	invalidate();
	return result;
}

bool ExtendableDataModelFilter::removeItem(const QModelIndex& index)
{
	auto result = m_model->removeItem(QSortFilterProxyModel::mapToSource(index));
	invalidate();
	return result;
}

QList<int> ExtendableDataModelFilter::getItemIds()
{
	return m_model->getItemIds();
}

QVariantMap ExtendableDataModelFilter::getVariantDefaultItem()
{
	return m_model->getVariantDefaultItem();
}

void ExtendableDataModelFilter::setColumns(QMap<Interface, QStringList> filteredColumns)
{
	m_filteredColumns = filteredColumns;
	invalidate();
}

void ExtendableDataModelFilter::setTreeParent(QModelIndex parentIndex)
{
	m_isParentSet = true;
	m_filterParentIndex = parentIndex;
	m_parentId = m_model->getItemId(m_filterParentIndex);
	parentIdChanged(m_parentId);
	invalidate();
}

void ExtendableDataModelFilter::setTreeParent(int parentRow)
{
	m_isParentSet = true;
	if(parentRow == -1)
		m_filterParentIndex = m_filterParentIndex.parent();
	else
		m_filterParentIndex = m_model->index(parentRow, 0, m_filterParentIndex);
	m_parentId = m_model->getItemId(m_filterParentIndex);
	parentIdChanged(m_parentId);
	invalidate();
}

bool ExtendableDataModelFilter::filterAcceptsRow(int source_row, const QModelIndex& source_parent) const
{
	if(!m_isParentSet)
		return true;
	return m_filterParentIndex == source_parent;
}

bool ExtendableDataModelFilter::filterAcceptsColumn(int source_column, const QModelIndex& source_parent) const
{
	if(m_filteredColumns.isEmpty())
		return true;
	auto columnToDataMap = m_model->getColumnToDataMap();
	auto dataItemDescr = columnToDataMap[source_column];
	auto iter = m_filteredColumns.find(dataItemDescr.first);
	return iter != m_filteredColumns.end() && iter.value().contains(dataItemDescr.second);
}
