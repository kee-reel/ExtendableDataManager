#pragma once

#include <QtCore>

#include "../../Common/DataExtention/idataextention.h"

struct TableItem
{
	int id;
	QMap<Interface, QVariantMap> dataChunks;
};

class ITableHandler
{
public:
	virtual ~ITableHandler() {}
	virtual bool SetRelation(Interface relationInterface, QVariantMap fields) = 0;
	virtual bool DeleteRelation(Interface relationInterface) = 0;

	virtual int AddItem(TableItem& item) = 0;
	virtual bool UpdateItem(TableItem& item) = 0;
	virtual bool DeleteItem(int id) = 0;

	virtual QList<TableItem> GetData() = 0;
	virtual TableItem GetItem(int id) = 0;
	virtual QPointer<IExtendableDataModel> GetModel() = 0;
	virtual QMap<Interface, QVariantMap> GetHeader() = 0;
};
