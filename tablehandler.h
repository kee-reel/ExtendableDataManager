#pragma once

#include <QMap>
#include <QtSql>
#include <QDebug>
#include <QVariant>

#include "../../Common/DataExtention/idataextention.h"
#include "../../Interfaces/i_async_database.h"
#include "model.h"
#include "itablehandler.h"

//! \addtogroup ExtendableDataManager_imp
//!  \{
class TableHandler : public QObject, public ITableHandler
{
	Q_OBJECT
public:
	QMap<int, QString> dataBaseTypesNames =
	{
		{QMetaType::Type::Int, "INTEGER"},
		{QMetaType::Type::UInt, "INTEGER"},
		{QMetaType::Type::LongLong, "INTEGER"},
		{QMetaType::Type::ULongLong, "INTEGER"},
		{QMetaType::Type::QString, "VARCHAR"},
		{QMetaType::Type::QDateTime, "INTEGER"},
		{QMetaType::Type::QByteArray, "BLOB"},
		{QMetaType::Type::Bool, "BOOLEAN"}
	};

	TableHandler(ReferenceInstancePtr<IAsyncDataBase> dataSource, Interface mainInterface);
	virtual ~TableHandler() {}

	inline QString TableName()
	{
		return tableName;
	}
	bool HasRelation(Interface relationInterface);

	bool CreateTable();
	bool SetRelation(Interface relationInterface, QVariantMap fields) override;
	bool DeleteRelation(Interface relationInterface) override;

	int AddItem(TableItem& item) override;
	bool UpdateItem(TableItem& item) override;
	bool DeleteItem(int id) override;

	QList<TableItem> GetData() override;
	TableItem GetItem(int id) override;
	QPointer<IExtendableDataModel> GetModel() override;
	QMap<Interface, QVariantMap> GetHeader() override;

private:
	QString GetInsertValuesString(QVariantMap &tableStruct, int id, QVariantMap& itemData);
	QString GetUpdateValuesString(QVariantMap &tableStruct, int id);
	QString GetUpdateValuesString(QVariantMap &tableStruct, int id, QVariantMap& itemData);

	bool IsDataSourceExists();
	bool IsTableExists(QString tableName);
	bool IsTableHasRightStructure(QString tableName, QVariantMap &tableStruct);
	void CombineWholeTableStruct();
	QString GetHeaderString(QVariantMap &tableStruct, bool createRelation = false);
	QString GetFieldsNames(QString tableName, QVariantMap &tableStruct, bool includeId = false);
	void InstallModel();
	QString convertIterfaceToTableName(const Interface& interface);

	void executeQuery(QSharedPointer<IQuery> query);

private slots:
	void onExecutedQuery(QUuid uuid);

private:
	ReferenceInstancePtr<IAsyncDataBase> dataSource;
	QSharedPointer<ExtendableDataModel> itemModel;
	QString tableName;
	bool isCreated;

	QVariantMap wholeTableStruct;
	QVariantMap coreTableStruct;
	QMap<Interface, QVariantMap> relationTablesStructs;
	QMap<QUuid, QSharedPointer<IQuery>> m_queries;
};
//!  \}

