#include "item.h"

Item::Item() :
	parentItem(nullptr)
{

}

Item::Item(TableItem& tableItem) :
	tableItem(tableItem),
	parentItem(nullptr)
{
	updateCombinedChunks();
}

Item::Item(Item *parent, Item *copy)
{
	parentItem = parent;

	if(!copy)
		return;

	tableItem = copy->tableItem;
	combinedChunks = copy->combinedChunks;
	updateCombinedChunks();
}

Item::Item(Item* parent, TableItem& item)
{
	parentItem = parent;
	tableItem = item;
	updateCombinedChunks();
}

Item::~Item()
{
	if(childItems.count())
		for(int i = 0; i < childItems.count(); i++)
			delete childItems[i];
}

void Item::DetachFromParent()
{
	if(!parentItem)
		return;

	parentItem->RemoveChild(this);
	parentItem = nullptr;
}

void Item::SetParent(Item *parent)
{
	parentItem = parent;
	tableItem.dataChunks[Interface("tree")]["parent"] = parent ? parent->GetId() : -1;
	tableItem.dataChunks[Interface("tree")]["position"] = parent ? parent->GetChildPosition(this) : 0;
}

void Item::SetChilds(QList<Item *> childs)
{
	foreach (auto child, childs)
	{
		AddChild(child, childItems.count());
	}
}

QList<Item*> Item::getChildren()
{
	return childItems;
}

void Item::AddChild(Item *child, int row)
{
	childItems.insert(row, child);
	child->SetParent(this);
}

void Item::RemoveChild(Item *child)
{
	childItems.removeOne(child);
}

void Item::RemoveChildAt(int row)
{
	if(childItems.count() < row)
		return;

	Item *child = childItems[row];
	child->SetParent(nullptr);
	childItems.removeAt(row);
}

QList<Interface> Item::GetChunksNames() const
{
	return tableItem.dataChunks.keys();
}

QList<QString> Item::GetChunkFieldNames(Interface interface) const
{
	return tableItem.dataChunks[interface].keys();
}

QList<QString> Item::GetCombinedChunksFieldNames() const
{
	QList<QString> names;
	for(int i = 0; i < combinedChunks.length(); ++i)
	{
		names.append(m_combinedChunkToDataMap[i].second);
	}
	return names;
}

QVariantMap Item::GetChunkData(Interface chunkInterface) const
{
	return tableItem.dataChunks[chunkInterface];
}

QVariant Item::GetChunkDataElement(int column) const
{
	return *combinedChunks.at(column);
}

void Item::SetChunkData(Interface chunkInterface, QVariantMap dataMap)
{
	tableItem.dataChunks[chunkInterface] = dataMap;
	updateCombinedChunks();
}

void Item::SetChunksData(const ExtendableItemDataMap& dataMap)
{
	for(auto iter = dataMap.begin(); iter != dataMap.end(); ++iter)
	{
		tableItem.dataChunks[iter.key()].insert(iter.value());
	}
	updateCombinedChunks();
}

const ExtendableItemDataMap& Item::getChunksData()
{
	return tableItem.dataChunks;
}

ExtendableItemDataMap Item::getChunksDataCopy() const
{
	ExtendableItemDataMap copy;
	for(auto iter = tableItem.dataChunks.begin(); iter != tableItem.dataChunks.end(); ++iter)
	{
		for(auto fieldIter = iter.value().begin(); fieldIter != iter.value().end(); ++fieldIter)
		{
			copy[iter.key()][fieldIter.key()] = fieldIter.value();
		}
	}
	return copy;
}

QString Item::GetChunkDataElementName(int column) const
{
	auto dataMapDescr = m_combinedChunkToDataMap[column];
	return dataMapDescr.second;
}

void Item::SetChunkDataElement(int column, QVariant data)
{
	if( column < combinedChunks.size() )
	{
		*combinedChunks[column] = data;
	}
	else
		qDebug() << "Can't set column" << column;
}

void Item::SetDecoration(QVariant pixmap)
{
	decoration = new QVariant(pixmap);
}

QVariant Item::GetDecoration()
{
	return *decoration;
}

void Item::updateCombinedChunks()
{
	combinedChunks.clear();
	m_dataMapToCombinedChunk.clear();
	m_combinedChunkToDataMap.clear();

	for(auto iter = tableItem.dataChunks.begin(); iter != tableItem.dataChunks.end(); ++iter)
	{
		for(auto chunkIter = iter.value().begin(); chunkIter != iter.value().end(); ++chunkIter)
		{
			m_dataMapToCombinedChunk[iter.key()][chunkIter.key()] = combinedChunks.size();
			m_combinedChunkToDataMap[combinedChunks.size()] = QPair<Interface, QString>(iter.key(), chunkIter.key());
			combinedChunks.append(&chunkIter.value());
		}
	}
}
