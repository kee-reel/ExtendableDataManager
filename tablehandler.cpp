#include "tablehandler.h"

TableHandler::TableHandler(ReferenceInstancePtr<IAsyncDataBase> dataSource, Interface mainInterface) :
	QObject()
{
	this->dataSource = dataSource;
	this->tableName = convertIterfaceToTableName(mainInterface);
	CreateTable();
}

bool TableHandler::HasRelation(Interface relationInterface)
{
	return relationTablesStructs.contains(relationInterface);
}

bool TableHandler::CreateTable()
{
	// Is name valid?
	if(tableName == nullptr || tableName == "")
	{
		qDebug() << "Tree name is empty!";
		return false;
	}

	coreTableStruct = {{"id", QVariant(0)}};

	if(IsTableExists(tableName))
	{
		// Is table has right structure.
		if(!IsTableHasRightStructure(tableName, coreTableStruct))
		{
			qDebug() << "Table" << tableName << "has wrong structure";
			return false;
		}
	}
	else
	{
		// Create table.
        QString queryStr = QString("CREATE TABLE IF NOT EXISTS %1 (%2)").arg(tableName, GetHeaderString(coreTableStruct));
		executeQuery(dataSource->executeQueryAsync(queryStr));
	}

	CombineWholeTableStruct();
	isCreated = true;
	return true;
}

bool TableHandler::SetRelation(Interface relationInterface, QVariantMap fields)
{
	// HACK: This kind of magic trick with "id" field made only for setting proper table header
	// because for setting relation I need "id" field in table but not for operating with data.
	fields.insert("id", QVariant(0));
    QString databaseRelationName = QString("r_%1_%2").arg(tableName, convertIterfaceToTableName(relationInterface));
	bool tableCreationNeeded = true;

	if(IsTableExists(databaseRelationName))
	{
		tableCreationNeeded = false;

		if(!IsTableHasRightStructure(databaseRelationName, fields))
		{
			tableCreationNeeded = true;
			DeleteRelation(relationInterface);
		}
	}

	if(tableCreationNeeded)
	{
		QString dataFields = GetHeaderString(fields, true);

		if(dataFields == "")
		{
			qDebug() << "Can't create relation!";
			return false;
		}

        QString queryStr = QString("CREATE TABLE IF NOT EXISTS %1 (%2)").arg(databaseRelationName, dataFields);
		executeQuery(dataSource->executeQueryAsync(queryStr));

	}

	fields.remove("id");
	relationTablesStructs.insert(relationInterface, fields);

	if(itemModel)
		itemModel->AttachRelation(relationInterface, fields);

	return true;
}

bool TableHandler::DeleteRelation(Interface relationInterface)
{
	qDebug() << "DeleteRelation";
    QString queryStr = QString("DROP TABLE  r_%1_%2").arg(tableName, convertIterfaceToTableName(relationInterface));
	executeQuery(dataSource->executeQueryAsync(queryStr));

	relationTablesStructs.remove(relationInterface);
	CombineWholeTableStruct();
	return true;
}

QString TableHandler::convertIterfaceToTableName(const Interface& interface)
{
	return QString(interface.iid()).replace('/', '_').replace('.', '_');
}

void TableHandler::executeQuery(QSharedPointer<IQuery> query)
{
	m_queries[query->uuid()] = query;
	connect(query->object(), SIGNAL(completed(QUuid)), this, SLOT(onExecutedQuery(QUuid)));
	query->exec();
}

void TableHandler::onExecutedQuery(QUuid uuid)
{
	m_queries.remove(uuid);
}

TableItem TableHandler::GetItem(int id)
{
	QString queryStr = QString("SELECT %1.id").arg(tableName);
	QStringList joinTables;
	QString tableRefPrefix = QString("r_%1_").arg(tableName);

	for(auto iter = relationTablesStructs.begin(); iter != relationTablesStructs.end(); ++iter)
	{
		queryStr.append(",");
		auto relationTableName = convertIterfaceToTableName(iter.key());
		queryStr.append( GetFieldsNames(tableRefPrefix+relationTableName, iter.value()) );
		joinTables.append(relationTableName);
	}

	queryStr.append( QString(" FROM %1 ").arg(tableName) );
	queryStr.append( QString(" ON %1.id = %2 ").arg(tableName).arg(id) );

	for(int i = 0; i < joinTables.count(); i++)
	{
        queryStr.append(QString(" LEFT JOIN r_%1_%2 ON %1.id = r_%1_%2.id").arg(tableName, joinTables[i]));
	}

	auto&& query = dataSource->executeQueryAsync(queryStr);
	executeQuery(query);
	auto&& values = query->values();
	auto&& value = values.at(0);

	TableItem buf;
	QString bufStr;
	int queryFieldNum;
	bufStr = "";
	queryFieldNum = 0;
	buf.id = value.at(queryFieldNum).toInt();
	++queryFieldNum;

	for(auto iter = relationTablesStructs.begin(); iter != relationTablesStructs.end(); ++iter)
	{

		for(auto fieldIter = iter.value().begin(); fieldIter != iter.value().end(); ++fieldIter)
		{
			buf.dataChunks[iter.key()][fieldIter.key()] = value.at(queryFieldNum);
			++queryFieldNum;
		}
	}

	return buf;
}

QList<TableItem> TableHandler::GetData()
{
	QString queryStr = QString("SELECT %1.id").arg(tableName);
	QStringList joinTables;
	QString tableRefPrefix = QString("r_%1_").arg(tableName);

	for(auto iter = relationTablesStructs.begin(); iter != relationTablesStructs.end(); ++iter)
	{
		auto tableName = convertIterfaceToTableName(iter.key());
		queryStr.append(",");
		queryStr.append( GetFieldsNames(tableRefPrefix+tableName, iter.value()) );
		joinTables.append(tableName);
	}

	queryStr.append( QString(" FROM %1 ").arg(tableName) );

	for(int i = 0; i < joinTables.count(); i++)
	{
        queryStr.append(QString(" LEFT JOIN r_%1_%2 ON %1.id = r_%1_%2.id").arg(tableName, joinTables[i]));
	}

	auto&& query = dataSource->executeQueryAsync(queryStr);
	executeQuery(query);
	auto&& values = query->values();
	QList<TableItem> itemInfoList;
	TableItem buf;
	QString bufStr;
	int queryFieldNum;
	for (auto& value : values)
	{
		bufStr = "";
		queryFieldNum = 0;
		buf.id = value.at(queryFieldNum).toInt();
		++queryFieldNum;

		for(auto iter = relationTablesStructs.begin(); iter != relationTablesStructs.end(); ++iter)
		{
			auto& dataCunk = buf.dataChunks[iter.key()];
			for(auto fieldIter = iter.value().begin(); fieldIter != iter.value().end(); ++fieldIter)
			{
				auto dbValue = value.at(queryFieldNum);
                switch(fieldIter.value().typeId())
				{
				case QMetaType::Type::QDateTime:
					dbValue = QDateTime::fromSecsSinceEpoch(dbValue.toULongLong(), Qt::TimeSpec::UTC);
					break;
				case QMetaType::Type::Bool:
					dbValue = dbValue.toBool();
					break;
				}
				dataCunk[fieldIter.key()] = dbValue;
				++queryFieldNum;
			}
		}

		itemInfoList.append(buf);
		buf.dataChunks.clear();
	}

	return itemInfoList;
}

void TableHandler::InstallModel()
{
	if(!itemModel)
		itemModel.reset(new ExtendableDataModel(this));

	auto relationStructIter = relationTablesStructs.begin();

	while(relationStructIter != relationTablesStructs.end())
	{
		itemModel->AttachRelation(relationStructIter.key(), relationStructIter.value());
		++relationStructIter;
	}

	itemModel->LoadData();
}

QPointer<IExtendableDataModel> TableHandler::GetModel()
{
	InstallModel();
	return itemModel.data();
}

int TableHandler::AddItem(TableItem& item)
{
	QString queryStr = QString("INSERT INTO %1 (id) VALUES (NULL)").arg(tableName);
	auto&& query = dataSource->executeQueryAsync(queryStr);
	executeQuery(query);
	int lastId = query->lastInsertId();
	qDebug() << "TableHandler::AddItem" << lastId;

	for(auto iter = item.dataChunks.begin(); iter != item.dataChunks.end(); ++iter)
	{
		QString valuesString = GetInsertValuesString(relationTablesStructs[iter.key()], lastId, iter.value());

		if(valuesString != "")
		{
			queryStr = "";
            queryStr.append(QString("INSERT INTO r_%1_%2 %3").arg(
                                tableName, convertIterfaceToTableName(iter.key()), valuesString)
			);
			executeQuery(dataSource->executeQueryAsync(queryStr));
		}
		else
		{
			qDebug() << "Empty string!";
		}
	}

	return lastId;
}

bool TableHandler::UpdateItem(TableItem& item)
{
	QString queryStr;
	for(auto iter = relationTablesStructs.begin(); iter != relationTablesStructs.end(); ++iter)
	{
		QString valuesString = GetUpdateValuesString(iter.value(), item.id);

		if(valuesString != "")
		{
			queryStr = "";
			queryStr.append(QString("UPDATE r_%1_%2 SET %3")
                    .arg(tableName, convertIterfaceToTableName(iter.key()), valuesString) );
			QList<QString> list = iter.value().keys();

			for(int i = 0; i < list.count(); ++i)
				list[i] = ":" + list[i].toUpper();

			QList<QVariant> values;
            for(auto& field : item.dataChunks[iter.key()].values())
			{
				switch(field.typeId())
				{
				case QMetaType::Type::QDateTime:
					values.append(field.toDateTime().toSecsSinceEpoch());
					break;
				case QMetaType::Type::Bool:
					values.append(field.toBool());
					break;
				default:
					values.append(field);
				}
			}
			executeQuery(dataSource->executeQueryAsync(queryStr, list, values));
		}
		else
		{
			qDebug() << "Empty string!";
		}
	}

	return true;
}

bool TableHandler::DeleteItem(int id)
{
	QString queryStr = QString("delete from %1 where id=%2").arg(tableName).arg(id);
	qDebug() << "Delete Task" << queryStr;
	executeQuery(dataSource->executeQueryAsync(queryStr));

	return true;
}

QMap<Interface, QVariantMap> TableHandler::GetHeader()
{
	return relationTablesStructs;
}

QString TableHandler::GetHeaderString(QVariantMap &tableStruct, bool createRelation)
{
	QString structStr = "";
	QVariantMap::Iterator i = tableStruct.begin();
	QVariantMap::Iterator lastElement = --tableStruct.end();

	while(i != tableStruct.end())
	{
		QString typeNameString = dataBaseTypesNames.contains(i.value().typeId()) ?
                dataBaseTypesNames[i.value().typeId()] : i.value().metaType().name();
        structStr.append(QString("%1 %2").arg(i.key(), typeNameString));

		if(i.key() == "id")
		{
			QString idAppendix = createRelation ?
			        QString(" REFERENCES %1(id) ON DELETE CASCADE").arg(tableName) :
			        " PRIMARY KEY AUTOINCREMENT";
			structStr.append(idAppendix);
		}

		if(i != lastElement)
			structStr.append(",");

		++i;
	}

	return structStr;
}

QString TableHandler::GetFieldsNames(QString tableName, QVariantMap &tableStruct, bool includeId)
{
	QString structStr = "";
	QVariantMap::Iterator i = tableStruct.begin();
	QVariantMap::Iterator lastElement = --tableStruct.end();

	while(i != tableStruct.end())
	{
		if(i.key() != "id" || includeId)
            structStr.append(QString("%1.%2").arg(tableName, i.key()));

		if(i != lastElement)
			structStr.append(",");

		++i;
	}

	return structStr;
}

QString TableHandler::GetInsertValuesString(QVariantMap &tableStruct, int id, QVariantMap &itemData)
{
	if(tableStruct.count() != itemData.count())
	{
		qDebug() << "Wrong size" << tableStruct.count() << itemData.count();
		return QString();
	}

	QString fieldNamesStr = "(id, ";
	QString valuesStr = QString("(%1, ").arg(QString::number(id));
	QVariantMap::Iterator structIter = tableStruct.begin();
	QVariantMap::Iterator lastElement = --tableStruct.end();
	auto dataIter = itemData.begin();

	while(structIter != tableStruct.end())
	{
		fieldNamesStr.append(structIter.key());
		QString buf;

        auto dataType = dataIter.value().typeId();
		if(dataType == QMetaType::Type::QString || dataType == QMetaType::Type::QByteArray)
			buf = QString("'%1'").arg(dataIter->toString());
		else if(dataType == QMetaType::Type::QDateTime)
			buf = QString("%1").arg(dataIter->toDateTime().toSecsSinceEpoch());
		else
			buf = dataIter->toString();

		valuesStr.append(buf);

		if(structIter != lastElement)
		{
			fieldNamesStr.append(",");
			valuesStr.append(",");
		}
		else
		{
			fieldNamesStr.append(")");
			valuesStr.append(")");
		}

		++structIter;
		++dataIter;
	}

    return QString("%1 VALUES %2").arg(fieldNamesStr, valuesStr);
}

QString TableHandler::GetUpdateValuesString(QVariantMap &tableStruct, int id)
{
	QString resultStr = "";
	QVariantMap::Iterator structIter = tableStruct.begin();
	QVariantMap::Iterator lastElement = --tableStruct.end();

	while(structIter != tableStruct.end())
	{
		resultStr.append(structIter.key()).append("=");

		if(structIter.value().typeId() == QMetaType::Type::QString || structIter.value().typeId() == QMetaType::Type::QByteArray)
			resultStr.append(QString(":%1").arg(structIter.key().toUpper()));
		else
			resultStr.append(":").append(structIter.key().toUpper());

		if(structIter != lastElement)
			resultStr.append(",");

		++structIter;
	}

	resultStr.append(" where id=").append(QString::number(id));
	return resultStr;
}

QString TableHandler::GetUpdateValuesString(QVariantMap &tableStruct, int id, QVariantMap &itemData)
{
	if(tableStruct.count() != itemData.count())
	{
		qDebug() << "Wrong size" << tableStruct.count() << itemData.count();
		return QString();
	}

	QString resultStr = "";
	QVariantMap::Iterator structIter = tableStruct.begin();
	QVariantMap::Iterator lastElement = --tableStruct.end();
	auto dataIter = itemData.begin();

	while(structIter != tableStruct.end())
	{
		resultStr.append(structIter.key()).append("=");

        auto dataType = dataIter.value().typeId();
		if(dataType == QMetaType::Type::QString || dataType == QMetaType::Type::QByteArray)
			resultStr.append(QString("'%1'").arg(dataIter.value().toString()));
		else
			resultStr.append(dataIter.value().toString());

		if(structIter != lastElement)
			resultStr.append(",");

		++structIter;
		++dataIter;
	}

	resultStr.append(" where id=").append(QString::number(id));
	return resultStr;
}

bool TableHandler::IsTableExists(QString tableName)
{
	QString queryStr = QString("pragma table_info(%1)").arg(tableName);
	auto&& query = dataSource->executeQueryAsync(queryStr);
	executeQuery(query);
	auto&& values = query->values();
	return !values.empty();
}

void TableHandler::CombineWholeTableStruct()
{
	wholeTableStruct.clear();
	//	wholeTableStruct.unite(relationTablesStructs[tableName]);
	auto i = relationTablesStructs.begin();

	while(i != relationTablesStructs.end())
	{
		wholeTableStruct.insert(i.value());
		++i;
	}
}

bool TableHandler::IsTableHasRightStructure(QString tableName, QVariantMap &tableStruct)
{
	QString queryStr = QString("pragma table_info(%1)").arg(tableName);
	auto&& query = dataSource->executeQueryAsync(queryStr);
	executeQuery(query);
	auto&& values = query->values();
	auto tableStructIter = tableStruct.begin();
	auto valuesIter = values.begin();
	while(tableStructIter != tableStruct.end())
	{
		if(valuesIter == values.end())
		{
			qDebug() << "Too few records!";
			return false;
		}

		auto name = valuesIter->at(1).toString();
		if(tableStructIter.key() != name)
		{
            qDebug() << QString("Field's '%1' name doesn't match for table %2").arg(name, tableName);
			return false;
		}

        auto typeId = tableStructIter.value().typeId();
		auto sourceTypeString = valuesIter->at(2).toString();
        QString targetTypeString = dataBaseTypesNames.contains(typeId) ?
                    dataBaseTypesNames[typeId] : tableStructIter.value().metaType().name();
		if(sourceTypeString != targetTypeString)
		{
            qDebug() << QString("Field's '%1' type doesn't match for table %2").arg(name, tableName);
			return false;
		}

		++tableStructIter;
		++valuesIter;
	}

	return true;
}
