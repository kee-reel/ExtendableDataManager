#include "model.h"
#include "modelfilter.h"

ExtendableDataModel::ExtendableDataModel(ITableHandler *tableHandler, QObject *parent) :
	m_isDataLoadRequired(true)
{
	this->tableHandler = tableHandler;
	coreRelationName = Interface("tree");
	QMap<QString, QVariant> newRelationStruct =
	{
		{"parent", -1},
		{"position", 0}
	};
	tableHandler->SetRelation(coreRelationName, newRelationStruct);
	rootItem.SetChunkData(coreRelationName, newRelationStruct);
	rootItem.SetId(-1);
}

ExtendableDataModel::~ExtendableDataModel()
{
}

ExtendableItemDataMap ExtendableDataModel::getHeader()
{
	auto data = rootItem.getChunksData();
	data.remove(coreRelationName);
	return data;
}

QPointer<IExtendableDataModelFilter> ExtendableDataModel::getFilter()
{
	return new ExtendableDataModelFilter(this, this);
}

const ExtendableItemDataMap& ExtendableDataModel::getDefaultItem()
{
	return rootItem.getChunksData();
}

int ExtendableDataModel::getItemId(const QModelIndex& index)
{
	if (!index.isValid())
		return rootItem.GetId();

	Item *item = static_cast<Item*>(index.internalPointer());
	return item->GetId();
}

const ExtendableItemDataMap& ExtendableDataModel::getItem(const QModelIndex& index)
{
	if (!index.isValid())
		return rootItem.getChunksData();

	Item *item = static_cast<Item*>(index.internalPointer());
	return item->getChunksData();
}

const ExtendableItemDataMap& ExtendableDataModel::getItem(int itemId)
{
	return internalList[itemId]->getChunksData();
}

bool ExtendableDataModel::hasItem(int itemId)
{
	return internalList.contains(itemId);
}

QVariantMap ExtendableDataModel::getVariantItem(int itemId)
{
	auto iter = internalList.find(itemId);
	if(iter == internalList.end())
		return QVariantMap();

	auto data = iter.value()->getChunksData();
	QVariantMap resultData;
	for(auto iter = data.begin(); iter != data.end(); ++iter)
	{
		auto fieldsValues = iter.value();
		QVariantMap fieldsMap;
		for(auto fieldIter = fieldsValues.begin(); fieldIter != fieldsValues.end(); ++fieldIter)
		{
			fieldsMap[fieldIter.key()] = fieldIter.value();
		}
		resultData[iter.key().iid()] = fieldsMap;
	}
	return resultData;
}

QVariantMap ExtendableDataModel::getFirstVariantItem()
{
	if(internalList.isEmpty())
	{
		return QVariantMap();
	}
	else
	{
		return getVariantItem(internalList.keys().first());
	}
}

const ExtendableItemDataMap &ExtendableDataModel::getFirstItem()
{
	if(internalList.isEmpty())
	{
		return rootItem.getChunksData();
	}
	else
	{
		return getItem(internalList.keys().first());
	}
}

QModelIndex ExtendableDataModel::getItemIndex(int itemId)
{
	auto iter = internalList.find(itemId);
	if(iter != internalList.end())
		return createIndex(iter.value()->GetRow(), 0, iter.value());
	else
		return QModelIndex();
}

int ExtendableDataModel::appendItem(const ExtendableItemDataMap& itemValues)
{
	return addItem(itemValues, rowCount(), QModelIndex());
}

int ExtendableDataModel::addItem(const ExtendableItemDataMap& itemValues, int row, QModelIndex parentIndex)
{
	Item *parentItem = !parentIndex.isValid() ? &rootItem : static_cast<Item*>(parentIndex.internalPointer());

	beginInsertRows(parentIndex, row, row);

	auto item = TableItem({-1, itemValues});

	for(auto iter = rootItem.getChunksData().begin(); iter != rootItem.getChunksData().end(); ++iter)
	{
		auto& fields = item.dataChunks[iter.key()];
		for(auto valueIter = iter.value().begin(); valueIter != iter.value().end(); ++valueIter)
		{
			if(!fields.contains(valueIter.key()))
			{
				fields[valueIter.key()] = valueIter.value();
			}
		}
	}

	Item *childItem = AddItem(row, item, parentItem);
	endInsertRows();
	return childItem->GetId();
}

int ExtendableDataModel::addVariantItem(const QVariantMap& itemValues, int row, int parentId)
{
	ExtendableItemDataMap convertedMap;
	for(auto iter = itemValues.begin(); iter != itemValues.end(); ++iter)
	{
		auto fieldsValues = iter.value().toMap();
		auto& fieldsMap = convertedMap[Interface(iter.key())];
		for(auto fieldIter = fieldsValues.begin(); fieldIter != fieldsValues.end(); ++fieldIter)
		{
			fieldsMap[fieldIter.key()] = fieldIter.value();
		}
	}
	auto parentIndex = getItemIndex(parentId);
	if(row == -1)
	{
		row = rowCount(parentIndex);
	}
	return addItem(convertedMap, row, parentIndex);
}

bool ExtendableDataModel::updateItem(int itemId, const ExtendableItemDataMap& itemValues)
{
	auto& item = internalList[itemId];
	if(!item)
		return false;

	QMap<Interface, QMap<QString, QVariant> > dataBefore = item->getChunksDataCopy();
	item->SetChunksData(itemValues);
	UpdateItem(*item, dataBefore);
	auto index = createIndex(item->GetRow(), 0, item);
	emit dataChanged(index, index);
	return true;
}

bool ExtendableDataModel::updateVariantItem(int itemId, const QVariantMap& itemValues)
{
	auto& item = internalList[itemId];
	if(!item)
		return false;

	ExtendableItemDataMap convertedMap;
	for(auto iter = itemValues.begin(); iter != itemValues.end(); ++iter)
	{
		auto fieldsValues = iter.value().toMap();
		auto& fieldsMap = convertedMap[Interface(iter.key())];
		for(auto fieldIter = fieldsValues.begin(); fieldIter != fieldsValues.end(); ++fieldIter)
		{
			fieldsMap[fieldIter.key()] = fieldIter.value();
		}
	}
	return updateItem(getItemIndex(itemId), convertedMap);
}

bool ExtendableDataModel::updateItem(const QModelIndex& index, const ExtendableItemDataMap& itemValues)
{
	if (!index.isValid())
		return false;

	Item item = *static_cast<Item*>(index.internalPointer());

	QMap<Interface, QMap<QString, QVariant> > dataBefore = item.getChunksDataCopy();

	ExtendableItemDataMap itemValuesCopy = itemValues;
	for(auto iter = dataBefore.begin(); iter != dataBefore.end(); ++iter)
	{
		auto& fields = itemValuesCopy[iter.key()];
		for(auto valueIter = iter.value().begin(); valueIter != iter.value().end(); ++valueIter)
		{
			if(!fields.contains(valueIter.key()))
			{
				fields[valueIter.key()] = valueIter.value();
			}
		}
	}

	item.SetChunksData(itemValuesCopy);
	UpdateItem(item, dataBefore);
	auto left = this->index(index.row(), 0, index.parent());
	auto right = this->index(index.row(), 8, index.parent());
	emit dataChanged(left, right, roleNames().keys());
	return true;
}

bool ExtendableDataModel::removeItem(int itemId)
{
	auto item = internalList[itemId];
	if(!item)
		return false;

	auto index = createIndex(item->GetRow(), 0, item);
	removeRows(index.row(), 1, index.parent());
	return true;
}

bool ExtendableDataModel::removeItem(const QModelIndex& index)
{
	if (!index.isValid())
		return false;

	removeRows(index.row(), 1, index.parent());
	return true;
}

QList<int> ExtendableDataModel::getItemIds()
{
	return internalList.keys();
}

QVariantMap ExtendableDataModel::getVariantDefaultItem()
{
	auto data = rootItem.getChunksData();
	QVariantMap resultData;
	for(auto iter = data.begin(); iter != data.end(); ++iter)
	{
		auto fieldsValues = iter.value();
		QVariantMap fieldsMap;
		for(auto fieldIter = fieldsValues.begin(); fieldIter != fieldsValues.end(); ++fieldIter)
		{
			fieldsMap[fieldIter.key()] = fieldIter.value();
		}
		resultData[iter.key().iid()] = fieldsMap;
	}
	return resultData;
}

void ExtendableDataModel::LoadData()
{
	if(!m_isDataLoadRequired)
		return;

	for(auto& item : internalList.values())
	{
		DeleteItem(item);
	}

	QMap<int, QMultiMap<int, Item*>> internalTree;
	QList<TableItem> managerList = tableHandler->GetData();

	for(auto& item : managerList)
	{
		Item *treeItem = new Item(item);
		Q_ASSERT(item.dataChunks.contains(coreRelationName));

		auto dataChunk = item.dataChunks[coreRelationName];
        internalTree[dataChunk["parent"].toInt()].insert(dataChunk["position"].toInt(), treeItem);
		internalList.insert(treeItem->GetId(), treeItem);
	}

	for(auto iter = internalTree.begin(); iter != internalTree.end(); ++iter)
	{
		auto findIter = internalList.find(iter.key());
		Item* parent = findIter != internalList.end() ? findIter.value() : &rootItem;
		parent->SetChilds(iter.value().values());
	}

	m_isDataLoadRequired = false;
	emit modelChanged();
}

bool ExtendableDataModel::AttachRelation(Interface relationInterface, QVariantMap fields)
{
	if(!rootItem.HasChunk(relationInterface))
	{
		rootItem.SetChunkData(relationInterface, fields);
		m_isDataLoadRequired = true;
	}
	return true;
}

QHash<int, QByteArray> ExtendableDataModel::roleNames() const
{
	auto roles = QAbstractItemModel::roleNames();
	QStringList data = rootItem.GetCombinedChunksFieldNames();
	data.append("id");
	data.append("children");

	for(int i = 0; i < data.size(); ++i)
	{
		roles[Qt::UserRole+i] = data[i].toUtf8();
	}
	return roles;
}

QVariant ExtendableDataModel::data(const QModelIndex &index, int role) const
{
	if (!index.isValid())
		return QVariant();

	Item *item = static_cast<Item*>(index.internalPointer());

	switch (role)
	{
	case Qt::CheckStateRole:
	{
		auto value = item->GetChunkDataElement(index.column());
		if(value.typeId() == QMetaType::Type::Bool)
		{
			return value.toBool() ? Qt::Checked : Qt::Unchecked;
		}
	}
	break;
	case Qt::DisplayRole:
	{	auto value = item->GetChunkDataElement(index.column());
		if(value.typeId() == QMetaType::Type::Bool)
			return "";
		else
			return item->GetChunkDataElement(index.column());
	}

	case Qt::EditRole:
		return item->GetChunkDataElement(index.column());
	}

	if(role >= Qt::UserRole)
	{
		auto column = role % Qt::UserRole;
		if(column < item->ColumnCount())
		{
			return item->GetChunkDataElement(column);
		}
		else
		{
			switch (column - item->ColumnCount())
			{
			case CUSTOM_ROLES::ID:
				return item->GetId();
				break;
			case CUSTOM_ROLES::CHILDREN:
				return item->ChildCount();
				break;
			default:
				Q_ASSERT(false);
			}
		}
	}
	else
	{
		return QVariant();
	}
}

bool ExtendableDataModel::setItemData(const QModelIndex &index, const QMap<int, QVariant> &roles)
{
	if (!index.isValid())
		return false;
	return QAbstractItemModel::setItemData(index, roles);
}

Qt::ItemFlags ExtendableDataModel::flags(const QModelIndex &index) const
{
	if (!index.isValid())
		return Qt::NoItemFlags | Qt::ItemIsDropEnabled;

	auto flags = Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled | Qt::ItemIsEnabled;

	Item *item = static_cast<Item*>(index.internalPointer());
	auto value = item->GetChunkDataElement(index.column());
	if(value.typeId() == QMetaType::Type::Bool)
	{
		flags |= Qt::ItemIsSelectable | Qt::ItemIsUserCheckable;
	}
	return IExtendableDataModel::flags(index) | flags;
}

QVariant ExtendableDataModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	switch (role)
	{
	case Qt::DisplayRole:
		return rootItem.GetChunkDataElementName(section);
		break;

	case Qt::ToolTipRole:
		return rootItem.GetId();
		break;

	case Qt::EditRole:
		return rootItem.GetChunkDataElement(section);
		break;

	default:
		return QVariant();
		break;
	}

	return QVariant();
}

QModelIndex ExtendableDataModel::index(int row, int column, const QModelIndex &parent) const
{
	if (!hasIndex(row, column, parent))
	{
		qDebug() << "No Index for" << row << column << parent;
		return QModelIndex();
	}

	const Item *parentItem;

	if (!parent.isValid())
		parentItem = &rootItem;
	else
		parentItem = static_cast<Item*>(parent.internalPointer());

	Item *childItem = parentItem->GetChildAt(row);

	if (childItem)
		return createIndex(row, column, childItem);
	else
		return QModelIndex();
}

QModelIndex ExtendableDataModel::parent(const QModelIndex &index) const
{
	if (!index.isValid())
		return QModelIndex();

	Item *childItem = static_cast<Item*>(index.internalPointer());

	if (childItem->ParentIsRoot() || !childItem->HasParent())
		return QModelIndex();

	Item *parentItem = childItem->GetParent();
	return createIndex(parentItem->GetRow(), 0, parentItem);
}

int ExtendableDataModel::rowCount(const QModelIndex &parent) const
{
	const Item *parentItem;
	parentItem = (!parent.isValid()) ? &rootItem : static_cast<const Item*>(parent.internalPointer());
	return parentItem->ChildCount();
}

int ExtendableDataModel::columnCount(const QModelIndex &parent) const
{
	if (parent.isValid())
		return static_cast<Item*>(parent.internalPointer())->ColumnCount();
	else
		return rootItem.ColumnCount();
}

bool ExtendableDataModel::insertRows(int row, int count, const QModelIndex &parent)
{
	Item *parentItem;
	parentItem = !parent.isValid() ? &rootItem : static_cast<Item*>(parent.internalPointer());

	if(row == -1 || row >= parentItem->ChildCount())
		row = parentItem->ChildCount();

	beginInsertRows(parent, row, row+count);
	auto item = TableItem({-1, rootItem.getChunksData()});
	AddItem(row, item, parentItem);
	endInsertRows();
	return true;
}

bool ExtendableDataModel::insertColumns(int column, int count, const QModelIndex &parent)
{
	return true;
}

bool ExtendableDataModel::removeRows(int row, int count, const QModelIndex &parent)
{
	beginRemoveRows(parent, row, row+count-1);
	Item *parentItem;
	parentItem = (!parent.isValid()) ? &rootItem : static_cast<Item*>(parent.internalPointer());
	Item *item = parentItem->GetChildAt(row);

	if(item)
		DeleteItem(item);

	endRemoveRows();
	return true;
}

bool ExtendableDataModel::moveRows(const QModelIndex &sourceParent, int sourceRow, int count, const QModelIndex &destinationParent,
        int destinationChild)
{
	qDebug() << "moveRows" << sourceParent << sourceRow << count << destinationParent << destinationChild;
	int sourceLast = sourceRow+count;
	Item *sourceParentItem = (!sourceParent.isValid()) ? &rootItem : static_cast<Item*>(sourceParent.internalPointer());
	Item *destinationParentItem = (!destinationParent.isValid()) ? &rootItem : static_cast<Item*>(destinationParent.internalPointer());
	Item *destinationChildItem = destinationParentItem->GetChildAt(destinationChild);
	qDebug() << sourceParentItem << destinationChildItem << destinationParentItem->ChildCount() << sourceParentItem->ChildCount();
	QList<Item*> movingItems;

	if(!beginMoveRows(sourceParent, sourceRow, sourceLast, destinationParent, destinationChild))
	{
		qDebug() << "Wrong move";
		return false;
	}

	for(int i = sourceRow; i <= sourceLast; ++i)
	{
		movingItems.append(sourceParentItem->GetChildAt(i));
		//        sourceParentItem->RemoveChildAt(i);
		//        destinationParentItem->AddChild(movingItems.last());
	}

	destinationChild = destinationChildItem ? destinationChildItem->GetRow() : destinationChild;

	for(int i = 0; i < movingItems.count(); ++i)
	{
		sourceParentItem->RemoveChild(movingItems[i]);
		destinationParentItem->AddChild(movingItems[i], destinationChild);
	}

	endMoveRows();
	return true;
}

bool ExtendableDataModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
	Item& item = (!index.isValid()) ? rootItem : *static_cast<Item*>(index.internalPointer());

	switch (role)
	{
	case Qt::CheckStateRole:
	{
		auto dataBefore = item.getChunksDataCopy();
		item.SetChunkDataElement(index.column(), value.toInt() == Qt::Checked);
		UpdateItem(item, dataBefore);
		emit dataChanged(index, index, {role});
	}
	break;
	case Qt::EditRole:
	{
		ExtendableItemDataMap dataBefore = item.getChunksDataCopy();
		item.SetChunkDataElement(index.column(), value);
		UpdateItem(item, dataBefore);
		emit dataChanged(index, index, {role});
	}
	}

	if(role >= Qt::UserRole)
	{
		auto column = role % Qt::UserRole;
		if(column < item.ColumnCount())
		{
			ExtendableItemDataMap dataBefore = item.getChunksDataCopy();
			item.SetChunkDataElement(column, value);
			UpdateItem(item, dataBefore);
			auto left = this->index(index.row(), 0, index.parent());
			auto right = this->index(index.row(), 8, index.parent());
			emit dataChanged(left, right, {role});
		}
	}

	return true;
}

QStringList ExtendableDataModel::mimeTypes() const
{
	QStringList types;
	types << "application/vnd.text.list";
	return types;
}

QMimeData *ExtendableDataModel::mimeData(const QModelIndexList &indexes) const
{
	qDebug() << "mimeData" << indexes.count();
	QMimeData *mimeData = new QMimeData();
	QByteArray encodedData;
	QDataStream stream(&encodedData, QIODevice::WriteOnly);
	stream << (quintptr)this;

	foreach (const QModelIndex &index, indexes)
	{
		if (index.isValid())
		{
			int row = index.row();
			int column = index.column();
			quintptr indexPtr = (quintptr)index.internalPointer();
			quintptr parentPtr = (quintptr)index.parent().internalPointer();
			qDebug() << row << column << indexPtr << parentPtr;
			stream << row << column << indexPtr << parentPtr;
		}
	}

	mimeData->setData("application/vnd.text.list", encodedData);
	return mimeData;
}

bool ExtendableDataModel::dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent)
{
	qDebug() << "dropMimeData" << row << column << parent;

	if (action == Qt::IgnoreAction)
		return true;

	if (!data->hasFormat("application/vnd.text.list"))
		return false;

	if (column > 0)
		return false;

	int beginRow;

	if (row != -1)
		beginRow = row;
	else if (parent.isValid())
		beginRow = parent.row();
	else
		beginRow = rowCount(QModelIndex());

	QByteArray encodedData = data->data("application/vnd.text.list");
	QDataStream stream(&encodedData, QIODevice::ReadOnly);
	qintptr modelPtr = reinterpret_cast<qintptr>(nullptr);
	stream >> modelPtr;
	QMap<quintptr, QMap<int, quintptr>> newItems;
	int rows = 0;

	while (!stream.atEnd())
	{
		int row;
		int column;
		quintptr idxPtr;
		quintptr parentPtr;
		stream >> row;
		stream >> column;
		stream >> idxPtr;
		stream >> parentPtr;
		qDebug() << row << column << idxPtr << parentPtr;
		newItems[parentPtr][row] = idxPtr;
		qDebug() << newItems.count();
		++rows;
	}

	if(modelPtr == (qintptr)this)
	{
		ReadSameModelMime(beginRow, row, parent, newItems);
	}
	else
	{
		qDebug() << "!Another model!";
		return false;
	}

	return true;
}

void ExtendableDataModel::ReadSameModelMime(int beginRow, int row, const QModelIndex &parent, QMap<quintptr, QMap<int, quintptr>> &newItems)
{
	QModelIndex bufIdx;
	QModelIndex blockFirstIdx;
	QModelIndex parentIdx;
	Item *blockFirstItem;

	if(!parent.isValid())
	{
		parentIdx = createIndex(-1, -1, newItems.begin().key());
		qDebug() << "That's it!";
	}
	else
		parentIdx = parent;

	QMap<quintptr, QMap<int, quintptr>>::Iterator parentI = newItems.begin();

	while(parentI != newItems.end())
	{
		qDebug() << "parentI" << parentI.key();
		QMap<int, quintptr> rows = parentI.value();
		QMap<int, quintptr>::Iterator rowsI = rows.begin();
		QMap<int, quintptr>::Iterator lastRowI = --rows.end();
		int itemsBlock = 0;
		int prevRow = -1;
		blockFirstItem = nullptr;

		while(rowsI != rows.end())
		{
			qDebug() << "rowsI" << rowsI.key();
			bufIdx = createIndex(rowsI.key(), 0, rowsI.value());

			if(itemsBlock == 0)
				blockFirstIdx = bufIdx;

			if( prevRow != -1 && (prevRow != rowsI.key()-1) )
			{
				qDebug() << "Second";
				Item *treeItem = (Item*)blockFirstIdx.internalPointer();
				moveRows(blockFirstIdx.parent(), treeItem->GetRow(), itemsBlock-1, parentIdx, beginRow);
				blockFirstIdx = bufIdx;
				itemsBlock = 0;
			}

			if(rowsI == lastRowI)
			{
				qDebug() << "First";
				Item *treeItem = (Item*)blockFirstIdx.internalPointer();
				moveRows(blockFirstIdx.parent(), treeItem->GetRow(), itemsBlock, parentIdx, beginRow);
				itemsBlock = 0;
			}

			prevRow = rowsI.key();
			++itemsBlock;
			++rowsI;
		}

		UpdateItemsPosition((Item*)parentI.key(), rows.begin().key());
		++parentI;
	}

	UpdateItemsPosition((Item*)parent.internalPointer(), (row == -1) ? 0 : row);
}

Item *ExtendableDataModel::AddItem(int row, TableItem& tableItem, Item *itemParent)
{
	if(!itemParent)
		itemParent = &rootItem;

	Item *newItem = new Item(itemParent, tableItem);

	if(itemParent)
		itemParent->AddChild(newItem, row);

	int newId = tableHandler->AddItem(newItem->getTableItem());
	newItem->SetId(newId);
	internalList.insert(newId, newItem);
	qDebug() << "New id:"<< newId;

	emit modelChanged();
	return newItem;
}

bool ExtendableDataModel::UpdateItem(Item &item, const ExtendableItemDataMap& dataBefore)
{
	tableHandler->UpdateItem(item.getTableItem());
	emit modelChanged();
	auto&& chunksData = item.getChunksData();
	emit itemUpdated(item.GetId(), dataBefore, chunksData);
	emit dataChanged(createIndex(item.GetRow(), 0, &item), createIndex(item.GetRow(), item.ColumnCount()-1, &item));
	return true;
}

bool ExtendableDataModel::UpdateItemsPosition(Item *parent, int from)
{
	if(!parent)
		parent = &rootItem;

	int to = parent->ChildCount();

	for(int i = from; i < to; ++i)
	{
		qDebug() << "ExtendableDataModel::UpdateItemsPosition" << parent->ChildCount() << to;
		qDebug() << "ExtendableDataModel::UpdateItemsPosition" << parent->ChildCount() << parent->GetChildAt(i);
		tableHandler->UpdateItem(parent->GetChildAt(i)->getTableItem());
	}
	return true;
}

bool ExtendableDataModel::DeleteItem(Item *task)
{
	if(!task)
		return false;

	task->DetachFromParent();
	DeleteFromManagerRecursive(task);
	emit modelChanged();
	return true;
}

void ExtendableDataModel::DeleteFromManagerRecursive(Item *task)
{
	for(auto& child : task->getChildren())
		DeleteFromManagerRecursive(child);

	internalList.remove(task->GetId());
	tableHandler->DeleteItem(task->GetId());
}
