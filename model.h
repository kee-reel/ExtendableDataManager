#pragma once

#include <QAbstractItemModel>
#include <QVariant>
#include <QVector>
#include <QDebug>
#include <QMimeData>
#include <QDataStream>
#include <QMap>
#include <QObject>

#include "../../Common/DataExtention/idataextention.h"
#include "../../Common/Plugin/referenceinstance.h"

#include "item.h"
#include "itablehandler.h"

enum CUSTOM_ROLES
{
	ID,
	CHILDREN
};

class ExtendableDataModel : public IExtendableDataModel
{
	Q_OBJECT
public:
	ExtendableDataModel(ITableHandler *tableHandler, QObject *parent = nullptr);
	virtual ~ExtendableDataModel() override;

	// IExtendableDataModel interface
public:
	QPointer<IExtendableDataModelFilter> getFilter() override;
	ExtendableItemDataMap getHeader() override;
	const ExtendableItemDataMap& getDefaultItem() override;
	int getItemId(const QModelIndex &index) override;
	const ExtendableItemDataMap& getItem(const QModelIndex &index) override;
	const ExtendableItemDataMap& getItem(int itemId) override;
	bool hasItem(int itemId) override;
	QList<int> getItemIds() override;

public slots:
	QVariantMap getVariantDefaultItem() override;
	QVariantMap getVariantItem(int itemId) override;
	QVariantMap getFirstVariantItem() override;
	const ExtendableItemDataMap& getFirstItem() override;
	QModelIndex getItemIndex(int itemId) override;
	int appendItem(const ExtendableItemDataMap& itemValues) override;
	int addItem(const ExtendableItemDataMap& itemValues, int row, QModelIndex parentIndex) override;
	int addVariantItem(const QVariantMap& itemValues, int row, int parentId) override;
	bool updateItem(int itemId, const ExtendableItemDataMap& itemValues) override;
	bool updateVariantItem(int itemId, const QVariantMap& itemValues) override;
	bool updateItem(const QModelIndex &index, const ExtendableItemDataMap& itemValues) override;
	bool removeItem(int itemId) override;
	bool removeItem(const QModelIndex &index) override;

signals:
	void headerChanged() override;
	void itemAdded(int itemId) override;
	void itemUpdated(int itemId, const ExtendableItemDataMap& before, const ExtendableItemDataMap& after) override;
	void itemRemoved() override;
	void modelChanged() override;

public:
	void LoadData();
	bool AttachRelation(Interface relationInterface, QVariantMap fields);

	QHash<int, QByteArray> roleNames() const override;
	virtual QVariant data(const QModelIndex &index, int role) const override;
	bool setItemData(const QModelIndex &index, const QMap<int, QVariant> &roles) override;
	virtual Qt::ItemFlags flags(const QModelIndex &index) const override;
	virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
	virtual QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const override;
	virtual QModelIndex parent(const QModelIndex &index) const override;
	virtual int rowCount(const QModelIndex &parent = QModelIndex()) const override;
	virtual int columnCount(const QModelIndex &parent = QModelIndex()) const override;
	Q_INVOKABLE virtual bool insertRows(int row, int count, const QModelIndex &parent) override;
	virtual bool insertColumns(int column, int count, const QModelIndex &parent) override;
	virtual bool removeRows(int row, int count, const QModelIndex &parent) override;
	virtual bool moveRows(const QModelIndex &sourceParent, int sourceRow, int count, const QModelIndex &destinationParent, int destinationChild) override;
	virtual bool setData(const QModelIndex &index, const QVariant &value, int role) override;
	virtual QStringList mimeTypes() const override;
	virtual QMimeData *mimeData(const QModelIndexList &indexes) const override;
	virtual bool dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent) override;

	Item *AddItem(int row, TableItem& tableItem, Item *taskParent = nullptr);
	bool UpdateItem(Item& item, const ExtendableItemDataMap& dataBefore);
	bool UpdateItemsPosition(Item* parent, int from);
	bool DeleteItem(Item *task);
	void DeleteFromManagerRecursive(Item *task);

	void ReadSameModelMime(int beginRow, int row, const QModelIndex &parent, QMap<quintptr, QMap<int, quintptr> > &newItems);
	QMap<int, QPair<Interface, QString> >& getColumnToDataMap()
	{
		return rootItem.getColumnToDataMap();
	}

private:
	Interface coreRelationName;
	ITableHandler* tableHandler;

	QMap<int, Item*> internalList;
	Item rootItem;
	bool m_isDataLoadRequired;
};
