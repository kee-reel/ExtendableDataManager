#pragma once


#include <QVector>
#include <QVariant>
#include <QDebug>
#include <QPixmap>
#include <QMap>

#include "itablehandler.h"

class Item
{
public:
	Item();
	Item(TableItem& tableItem);
	Item(Item *parent, Item *copy);
	Item(Item *parent, TableItem& tableItem);
	~Item();

	// Columns and rows
	inline int ColumnCount() const
	{
		return combinedChunks.size();
	}
	inline int GetRow()
	{
		return parentItem ? parentItem->GetChildPosition(this) : 0;
	}

	// Childs
	void DetachFromParent();
	void SetChilds(QList<Item *> childs);
	QList<Item *> getChildren();

	void AddChild(Item *child,
#ifdef Q_OS_ANDROID
	        int row = INT_MAX);
#else
	        int row = INT32_MAX);
#endif
	void RemoveChild(Item *child);
	void RemoveChildAt(int row);
	inline Item *GetChildAt(int row) const
	{
		return childItems.count() > row ? childItems[row] : nullptr;
	}
	inline int ChildCount() const
	{
		return childItems.count();
	}
	inline int GetChildPosition(Item* item) const
	{
		return childItems.indexOf(item);
	}
	inline bool HasParent() const
	{
		return parentItem != nullptr;
	}
	inline bool ParentIsRoot() const
	{
		return parentItem ? !parentItem->HasParent() : false;
	}
	inline Item *GetParent() const
	{
		return parentItem;
	}

	// Data
	inline bool HasChunk(Interface chunkInterface)
	{
		return tableItem.dataChunks.contains(chunkInterface);
	}

	QList<Interface> GetChunksNames() const;
	QList<QString> GetChunkFieldNames(Interface interface) const;
	QList<QString> GetCombinedChunksFieldNames() const;

	QVariantMap GetChunkData(Interface chunkInterface) const;
	void SetChunkData(Interface chunkInterface, QVariantMap dataMap);

	void SetChunksData(const ExtendableItemDataMap& dataMap);
	const ExtendableItemDataMap& getChunksData();
	ExtendableItemDataMap getChunksDataCopy() const;

	QString GetChunkDataElementName(int column) const;

	QVariant GetChunkDataElement(int column) const;
	void SetChunkDataElement(int column, QVariant data);

	QVariant GetDecoration();
	void SetDecoration(QVariant pixmap);

	inline int GetId() const
	{
		return tableItem.id;
	}
	inline void SetId(int id)
	{
		tableItem.id = id;
	}
	TableItem& getTableItem()
	{
		return tableItem;
	}
	QMap<int, QPair<Interface, QString> >& getColumnToDataMap()
	{
		return m_combinedChunkToDataMap;
	}

private:
	void SetParent(Item *parent);
	void updateCombinedChunks();

private:
	TableItem tableItem;
	Item *parentItem;
	QVector<bool> chunksEnableState;
	QMap<Interface, QMap<QString, int> > m_dataMapToCombinedChunk;
	QMap<int, QPair<Interface, QString> > m_combinedChunkToDataMap;
	QList<QVariant*> combinedChunks;

	QVariant *decoration;
	QList<Item*> childItems;
};

